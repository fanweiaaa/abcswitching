/**
 * Created by fwlst on 2014/10/27.
 */

$(function(){
    $(".main").slider();
});



(function($){
    $.fn.slider = function(){
        return this.each(function(){
            var $this = $(this);
            var $head = $this.find(".head li"),
                $bottom = $this.find(".bottom ul"),
                $next = $this.find(".head .next"),
                $prev = $this.find(".head .prev");
            $head.hover(function(){
                var index = 0;
                for(var i = 0; i < $head.length; i++){
                    if($head[i] === this){
                        index = i;
                        break
                    };
                };
                $head.css({
                    borderBottom:"1px solid #999999",
                    background:"#f4f4f4"
                });
                $($head[index]).css({
                    borderBottom:0,
                    background:"#FFFFFF"
                });
                $bottom.hide();
                $($bottom[index]).show();

            });

            var index = 0;

            function location(a,b){
                for(var i = 0; i < $bottom.length; i++){
                    if($($bottom[i]).css("display") === "block"){
                        index = i;
                        break
                    };
                };
            };


            $next.click(function(){
                location();
                if(index === $bottom.length - 1){
                    index = -1;
                };
                $head.css({
                    borderBottom:"1px solid #999999",
                    background:"#f4f4f4"
                })
                $($head[index + 1]).css({
                    borderBottom:0,
                    background:"#FFFFFF"
                });
                $bottom.hide();
                $($bottom[index + 1]).show();
            });
            $prev.click(function(){
                location();
                if(index === 0){
                    index = $bottom.length;
                };
                $head.css({
                    borderBottom:"1px solid #999999",
                    background:"#f4f4f4"
                });
                $($head[index - 1]).css({
                    borderBottom:0,
                    background:"#FFFFFF"
                });
                $bottom.hide();
                $($bottom[index - 1]).show();

            });

        });
    }
})(jQuery);